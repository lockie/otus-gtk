
all: chat

chat: main.o ui.o
	$(CC) $(CFLAGS) -Wall -Wextra -Wpedantic -std=c2x `pkg-config --libs gtk+-3.0 libsoup-3.0 json-glib-1.0` $^ -o $@

%.o: %.c
	$(CC) -c $(CFLAGS) -Wall -Wextra -Wpedantic -std=c2x `pkg-config --cflags gtk+-3.0 libsoup-3.0 json-glib-1.0` $^ -o $@

ui.c: ui.gresource.xml ui.glade
	glib-compile-resources --generate-source --target=$@ $<

clean:
	$(RM) chat *.o ui.c core ui.glade~

.PHONY: all clean
