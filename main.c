#include <stdlib.h>
#include <glib/gprintf.h>
#include <gtk/gtk.h>
#include <libsoup/soup.h>
#include <json-glib/json-glib.h>


static GtkBuilder* builder;

int main(int argc, char** argv)
{
    gtk_init(&argc, &argv);

    builder = gtk_builder_new_from_resource("/ui/ui.glade");
    gtk_builder_connect_signals(builder, NULL);

    GtkWidget* window = GTK_WIDGET(gtk_builder_get_object(builder, "chat"));
    gtk_widget_show_all(window);

    gtk_main();

    g_object_unref(builder);
    return EXIT_SUCCESS;
}

static gchar* get_input_line()
{
    GtkWidget* view = GTK_WIDGET(gtk_builder_get_object(builder, "input"));
    GtkTextBuffer* buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(view));
    GtkTextIter start, end;
    gtk_text_buffer_get_bounds(buffer, &start, &end);
    return gtk_text_buffer_get_text(buffer, &start, &end, FALSE);
}

static void toggle_spinner(gboolean show)
{
    GtkWidget* dialog = GTK_WIDGET(gtk_builder_get_object(builder, "spinner"));
    if(show)
        gtk_widget_show(dialog);
    else
        gtk_widget_hide(dialog);
}

static void report_error(const gchar* title, const gchar* text_format, ...)
{
    g_autofree gchar* text = NULL;
    va_list args;
    va_start(args, text_format);
    g_vasprintf(&text, text_format, args);

    GtkWidget* messagebox = gtk_message_dialog_new(
        NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
        "%s", title);
    gtk_message_dialog_format_secondary_text(
        GTK_MESSAGE_DIALOG(messagebox),
        "%s", text);
    gtk_dialog_run(GTK_DIALOG(messagebox));
    gtk_widget_destroy(messagebox);
}

static gchar* parse_response(GBytes* response)
{
    gsize resp_size;
    gconstpointer data = g_bytes_get_data(response, &resp_size);
    g_autoptr(JsonParser) parser = json_parser_new();
    g_autoptr(GError) error = NULL;
    json_parser_load_from_data(parser, data, resp_size, &error);

    if(error)
    {
        report_error("Error parsing response", error->message);
        return NULL;
    }

    JsonObject* root_obj = json_node_get_object(json_parser_get_root(parser));
    if(!root_obj)
    {
        report_error("Error parsing response", "Unexpected response structure");
        return NULL;
    }

    gchar* result = g_strdup(
        json_object_get_string_member(root_obj, "predictions"));
    if(!result)
    {
        report_error("Error parsing response", "Invalid 'predictions' field");
        return NULL;
    }
    return result;
}

static void response_callback(GObject*, GAsyncResult* res, gpointer data)
{
    toggle_spinner(FALSE);

    g_autoptr(SoupSession) session = data;
    g_autoptr(GError) error = NULL;
    g_autoptr(GBytes) response = soup_session_send_and_read_finish(
        session, res, &error);
    if(error)
    {
        report_error("Error making HTTP request", error->message);
        return;
    }

    SoupMessage* msg = soup_session_get_async_result_message(session, res);
    SoupStatus code = soup_message_get_status(msg);
    if(code != SOUP_STATUS_OK)
    {
        report_error("Error making HTTP request", "Unexpected status %d", code);
        return;
    }

    g_autofree gchar* reply = parse_response(response);
    if(!reply)
        return;

    GtkTextView* out = GTK_TEXT_VIEW(gtk_builder_get_object(builder, "output"));
    GtkTextBuffer* buffer = gtk_text_view_get_buffer(out);
    gtk_text_buffer_set_text(buffer, reply, -1);
}

#define CHAT_URL \
    "https://api.aicloud.sbercloud.ru/public/v1/public_inference/gpt3/predict"

static void send_chat_request(const gchar* line)
{
    SoupSession* session = soup_session_new();
    soup_session_set_user_agent(session, "libsoup");

    g_autoptr(SoupMessage) msg = soup_message_new(SOUP_METHOD_POST, CHAT_URL);

    GString* json = g_string_new(NULL);
    g_string_printf(json, "{\"text\": \"%s\"}", line);
    g_autoptr(GBytes) body = g_string_free_to_bytes(json);
    soup_message_set_request_body_from_bytes(msg, "application/json", body);

    soup_message_headers_append(
        soup_message_get_request_headers(msg),
        "Origin", "https://russiannlp.github.io");

    soup_session_send_and_read_async(
        session, msg, G_PRIORITY_DEFAULT, NULL, response_callback, session);
}

void do_chat(GtkWidget*, gpointer)
{
    toggle_spinner(TRUE);

    g_autofree gchar* line = get_input_line();

    send_chat_request(line);
}
